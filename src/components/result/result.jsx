import React from "react";
import PropTypes from "prop-types";
import { Media } from "react-bootstrap";

class ResultComponent extends React.Component {
  getEmailSubjectForLegislator(legislator) {
    return `Custom Subject for ${
      legislator.party === "D" ? "Democratic" : "Republican"
    } Legislator`;
  }

  getEmailBodyForLegislator(legislator) {
    return `Custom Body for ${
      legislator.party === "D" ? "Democratic" : "Republican"
    } Legislator ${legislator.name}`;
  }

  render() {
    const { legislators, district, onBack } = this.props;

    let prop2Pct = undefined;
    if (legislators.length > 0 && legislators[0]) {
      prop2Pct = legislators[0]["yes"];
    }

    return (
      <div>
        <div className="">
          <div className="" style={{
            display: 'flex',
            justifyContent: 'space-between'
          }}>
            <div>
              <h2>District: {district}</h2>
            </div>
              <button className="btn btn-primary btn-sm" onClick={onBack}>
                Start Over
              </button>
          </div>
          <h4>
            Prop 2: <strong>{prop2Pct}%</strong>
          </h4>
        </div>
        <div className="list-group">
          {legislators.map(legislator => (
            <div key={legislator.id} className="list-group-item" style={{
              borderLeftWidth: '2',
              borderLeftColor: (legislator.party === 'R') ? 'red' : 'blue'
            }}>
              <div className="list-group-item-heading">{legislator.name}</div>
              <Media>
                <Media.Left>
                  <img
                    height={64}
                    src={legislator.images.original}
                    alt={legislator.name}
                  />
                </Media.Left>
                <Media.Body>
                  {legislator.email &&
                    <div>
                      <label><span className="glyphicon glyphicon-envelope" aria-hidden="true" />&nbsp;</label>
                      <a
                        href={`mailto:${
                          legislator.email
                        }?subject=${this.getEmailSubjectForLegislator(
                          legislator
                        )}&body=${this.getEmailBodyForLegislator(legislator)}`}
                      >
                        {legislator.email}
                      </a>
                    </div>
                  }
                  {(legislator.statehouse_phone || legislator.home_phone) && (
                    <div>
                      <label><span className="glyphicon glyphicon-earphone" aria-hidden="true" />&nbsp;</label>
                      <a href={`tel:${legislator.statehouse_phone || legislator.home_phone}`}>
                        {legislator.statehouse_phone || legislator.home_phone}
                      </a>
                    </div>
                  )}
                </Media.Body>
              </Media>
            </div>
          ))}
        </div>
      </div>
    );
  }
}

ResultComponent.propTypes = {
  legislators: PropTypes.array.isRequired,
  district: PropTypes.number.isRequired,
  onBack: PropTypes.func.isRequired
};

export default ResultComponent;
