import React from 'react'
import PropTypes from 'prop-types'
import LegislatorService from '../../services/LegislatorService';

class SearchComponent extends React.Component {

    constructor(props) {
        super(props)

        this.state = {
            search: '',
            loading: false
        }
    }

    onSubmit() {
        const {
            search
        } = this.state

        if (search === '') {
            return;
        }

        this.setState({
            loading: true
        })

        LegislatorService.getByDistrict(search)
            .then((legislators) => {
                this.setState({
                    loading: false
                })
                this.props.onFindLegislators(legislators, parseInt(search))
            },
            () => {
                this.setState({
                    loading: false
                })
            })
    }

    render() {
        const {
            search,
            loading
        } = this.state

        return (
            <div>
                <form 
                className=""
                onSubmit={(e) => {
                    e.preventDefault()
                    e.nativeEvent.stopImmediatePropagation()
                    this.onSubmit()
                }}> 
                    <div className="form-group">
                        <label htmlFor="district-number">Select your Legislative district # to get your Representatives Contact information</label>
                        <input id="district-number" className="form-control" type="number" min={1} max={35} value={search} onChange={(e) => {
                            this.setState({
                                search: e.target.value
                            })
                        }}/>
                    </div>
                    <button type="submit" className="btn btn-primary" disabled={search === '' || loading}>Search</button>
                </form>
            </div>
        )
    }
}

SearchComponent.propTypes = {
    onFindLegislators: PropTypes.func.isRequired,
}

export default SearchComponent