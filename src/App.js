import React, { Component } from 'react';
import 'bootstrap/dist/css/bootstrap.css'
import './App.css';
import { SearchComponent } from './components/search';
import { ResultComponent } from './components/result';

class App extends Component {

  constructor(props) {
    super(props)

    this.state = {
      route: 'search',
      legislators: [],
      district: undefined,
    }
  }

  render() {
    const {
      route,
      legislators,
      district
    } = this.state

    let component = null

    switch (route) {
      case 'search':
        component = <SearchComponent
          onFindLegislators={(legislators, district) => {
            this.setState({
              legislators,
              district,
              route: 'result'
            })
          }}
        />
        break;
      case 'result':
          component = <ResultComponent legislators={legislators} district={district} onBack={() => {
            this.setState({
              route: 'search'
            })
          }} />
          break;
      default:
        component = null
    }

    return (
      <div style={{
        width: '100%',
        height: '530px',
        padding: '25px',
        overflow: 'hidden'
      }}>
        <div className="container-fluid">
          {component}
        </div>
      </div>
    );
  }
}

export default App;
