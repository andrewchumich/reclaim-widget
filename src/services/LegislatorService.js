import Airtable from 'airtable/lib/airtable'

class LegislatorService {
    base = undefined

    constructor() {
        this.base = new Airtable({
            apiKey: process.env.REACT_APP_AIRTABLE_API_KEY
        }).base('appCsE4tUNjjFq8Md')
    }

    getPartyByName(name) {
        if (name.indexOf('(R)') > -1) {
            return 'R'
        } else if (name.indexOf('(D)') > -1) {
            return 'D'
        } else {
            return 'N/A'
        }
    }

    getImages(airtableImages) {
        let images = {
            thumbnail: undefined,
            original: undefined
        }
        if (airtableImages.length > 0) {
            images.thumbnail = airtableImages[0].thumbnails.small.url
            images.original = airtableImages[0].url
        }

        return images
    }

    mapFieldsToLegilator(fields, id) {
        return {
            id,
            original_data: fields,
            district: fields.discrict,
            email: fields.email,
            home_phone: fields['home phone'],
            mailing_address: fields['mailing address'],
            name: fields.name,
            no: fields.no,
            statehouse_phone: fields['statehouse phone'],
            total_votes: fields['total votes'],
            yes: fields.yes,
            party: this.getPartyByName(fields.name),
            images: this.getImages(fields['Image']) 
        }
    }

    getByDistrict(discrict) {
        return new Promise((resolve, reject) => {            
            this.base('Legislator Table').select({
                filterByFormula: `{district} = ${discrict}`
            }).firstPage((err, records) => {
                if (err) { reject(err) }
                
                resolve(records.map((record) => {
                    return this.mapFieldsToLegilator(record.fields, record.id)
                }));
            })
        })
    }

}

export default new LegislatorService()